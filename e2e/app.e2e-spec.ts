import { MoshPage } from './app.po';

describe('mosh App', () => {
  let page: MoshPage;

  beforeEach(() => {
    page = new MoshPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
