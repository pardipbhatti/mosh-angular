import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-votter',
  templateUrl: './votter.component.html',
  styleUrls: ['./votter.component.css']
})
export class VotterComponent implements OnInit {

  @Input('is-votes') totalLikes = 0;
  @Input('is-favorite') iLike = false;
  @Input('is-id') isId = '';

  @Output('change-event') change = new EventEmitter()

  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.iLike = !this.iLike;
    this.totalLikes += this.iLike ? 1 : -1;
    this.change.emit({ likeValue: this.totalLikes, userid: this.isId });
  }

}
