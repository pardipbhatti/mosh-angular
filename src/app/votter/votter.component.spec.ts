import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VotterComponent } from './votter.component';

describe('VotterComponent', () => {
  let component: VotterComponent;
  let fixture: ComponentFixture<VotterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VotterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VotterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
