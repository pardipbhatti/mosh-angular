import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IsfavoriteComponent } from './isfavorite.component';

describe('IsfavoriteComponent', () => {
  let component: IsfavoriteComponent;
  let fixture: ComponentFixture<IsfavoriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IsfavoriteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IsfavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
