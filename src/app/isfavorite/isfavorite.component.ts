import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-isfavorite',
  templateUrl: './isfavorite.component.html',
  styleUrls: ['./isfavorite.component.css']
})
export class IsfavoriteComponent implements OnInit {


  @Input('is-favorite') isFavorite = false;
  @Input('is-id') isId = '';

  @Output('favorite-chnage') change = new EventEmitter();

  onClick() {
    this.isFavorite = !this.isFavorite;
    this.change.emit({ newValue: this.isFavorite, title: this.isId });
  }

  ngOnInit() {

  }

}
