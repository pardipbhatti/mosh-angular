import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  isActive = true;

  posts = [
    {
      id: 1,
      title: 'Gagan',
      isFavorite: true,
      votes: 3
    },
    {
      id: 2,
      title: 'Pardip',
      isFavorite: false,
      votes: 5
    },
    {
      id: 3,
      title: 'Sandeep',
      isFavorite: true,
      votes: 10
    },
  ]

  onFavoriteChange($event) {
    console.log($event);
  }

  onlikeChange($event) {
    console.log($event);
  }
}
