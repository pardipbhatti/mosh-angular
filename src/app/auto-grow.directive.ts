import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appAutoGrow]',
  host: {
    '(focus)': 'onFocus()',
    '(blur)': 'onBlur()'
  }
})
export class AutoGrowDirective {

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  onFocus() {
    this.renderer.setStyle(this.el.nativeElement, 'width', '120%');
  }

  onBlur() {
    this.renderer.setStyle(this.el.nativeElement, 'width', '100%');
  }

}
