import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AutoGrowDirective } from './auto-grow.directive';
import { IsfavoriteComponent } from './isfavorite/isfavorite.component';
import { VotterComponent } from './votter/votter.component';

@NgModule({
  declarations: [
    AppComponent,
    AutoGrowDirective,
    IsfavoriteComponent,
    VotterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
